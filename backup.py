"""
====================================================================
s3backup tool by Nathan "Chompy" Ogden
====================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
=====================================================================
"""

# Basic settings here....
tmp_dir = "/tmp"
s3cmd_path = "s3cmd"
mysqldump_path = "mysqldump"
pgsqldump_path = "pg_dump"

# End settings  ==============================

# Begin Program ==============================

# import required modules
import os, sys, shutil, errno, datetime, time
from subprocess import Popen
from subprocess import PIPE

# make sure a config file is provided
if len(sys.argv) <= 1:
  print "No config file specified."
  sys.exit('Usage: %s config-file' % sys.argv[0])

# make sure we can open config file
try:
  confFile = open(sys.argv[1], "r")
except IOError:
  sys.exit("Error: Unable to open config file.")

# read the config file  
conf = confFile.read()
confFile.close()

# default config values
config = {
  'name'                : 'unnamed',
  'keep_last_backups'   : 3,
  'database'            : None,
  'mysqlhost'           : 'localhost',
  'mysqluser'           : 'root',
  'mysqlpass'           : 'root',
  'backupto'            : os.path.dirname(os.path.realpath(__file__)) + "/backups/",
  'bucket'              : None
}

# parse config file
for i in conf.split("\n"):
  if not i.strip(): continue
  if i.strip()[0] == "#": continue
  split = i.strip().split("=")
  config[split[0].strip()] = split[1].strip()

# Get the path used for temporarly storage
tmp_path = tmp_dir + "/" + config['name']

# Remove old temp folder.
if os.path.exists(tmp_path):
  shutil.rmtree(tmp_path)

# Create temp path
os.mkdir(tmp_path)

# backup each set of files listed in config
print "Backing up files..."
for i in config['backup'].split(","):
  path = i.strip()
  if not os.path.exists(path): continue

  lastPath = os.path.normpath(path).split("/")
  lastPath = lastPath[len(lastPath) - 1]
  
  # try to copy as directory
  try:
    shutil.copytree(path, tmp_path + "/" + lastPath)
  except OSError as exc:
  
    # try to copy as single file
    if exc.errno == errno.ENOTDIR:
      shutil.copy(path, tmp_path + "/" + lastPath)
    # otherwise some other problem is preventing copying, raise exception
    else:
      raise

# backup mysql if provided.
if "mysqldbs" in config and config['mysqldbs']:
  print "Backing up MySQL database(s)..."
  
  # go through each database listed
  for i in config['mysqldbs'].split(","):
    
    # get database name
    database = i.strip()
    
    # run mysqldump from shell using config file parameters
    os.system("%s --host=%s -u%s -p%s %s > %s" % (
      mysqldump_path,
      config['mysqlhost'],
      config['mysqluser'],
      config['mysqlpass'],
      database,
      tmp_path + "/" + database + ".sql" 
    ))

# backup postgres if provided.
if "pgsqldbs" in config and config['pgsqldbs']:
  print "Backing up PostgreSQL database(s)..."
  
  # go through each database listed
  for i in config['pgsqldbs'].split(","):
    
    # get database name
    database = i.strip()
    
    # run pg_dump from shell using config file parameters
    os.system("PGPASSWORD='%s' %s --host=%s --username=%s %s > %s" % (
      config['pgsqlpass'],
      pgsqldump_path,
      config['pgsqlhost'],
      config['pgsqluser'],
      database,
      tmp_path + "/" + database + ".pgsql" 
    ))
  
# begin taring up files + database
print "Compressing backup..."

# get current time to use in file name
now = datetime.datetime.now()

# form filename time string
date_filename = str(now.month) + str(now.day) + str(now.year)

# change directory to temp folder
os.chdir(tmp_path + "/..")

# form filename string
thisFile = config['name']+"_"+date_filename+".tar.gz"

# run tar from shell and compress our backup
os.system("tar cfz %s %s" % (thisFile, config['name']))

# move backup to backup directory
backupPath = config['backupto'] + "/" + config['name']
 
# attempt to create the backup directory if it does not exist
if not os.path.exists(config['backupto']):
  os.mkdir(config['backupto'])
if not os.path.exists(backupPath):
  os.mkdir(backupPath)

# if file with same name already exists delete it
# (this would prevent making more the one backup daily
# if you need more backups daily make changes to the 
# date_filename var)
if os.path.exists(backupPath + "/" + thisFile):
  os.remove(backupPath + "/" + thisFile)

# more compressed back to our backup path
shutil.move(thisFile, backupPath)

# remove our temporary folder
shutil.rmtree(tmp_path)

# prune older backups (number of backups to keep can
# be choosen in config file)
print "Clearing old backups..."

# get directory listing
backups = os.listdir(backupPath)

# make an array, it'll be use to store the most recent file names
saveFile = []

# loop X number of times where X is the number of backups to keep
if (int(config['keep_last_backups']) > 0):
  for x in range(int(config['keep_last_backups'])):

    # clear vars used to determine recent files
    mostRecentFile = ""
    mostRecent = 0
    
    # loop through all backups in backup dir
    for i in backups:
    
      # ignore files that don't contain the name of this backup
      if i.find(config['name'].strip()) == -1: continue
      
      # get file creation time
      thisTime = os.path.getmtime(backupPath + "/" + i)
      
      # make sure this file isn't already flagged as a recent
      # backup and  if this file has a more recent time then 
      # last set a var to let us know
      if thisTime > mostRecent and not i in saveFile:
        mostRecent = thisTime
        mostRecentFile = i
    
    # after the loop append the latest most recent file
    if mostRecentFile:
      saveFile.append(mostRecentFile)
      
  # loop through all backups one more time
  # this time we'll delete anything that isn't
  # in our list of recent files
  for i in backups:
   
    # ignore files that don't contain the name of this backup 
    if i.find(config['name'].strip()) == -1: continue
    
    # ignore recent files
    if i in saveFile: continue
    
    # ignore this backup that we just made
    if i == thisFile: continue
    
    # remove the backup file
    os.remove(backupPath + "/" + i)

# prune S3 files (if a bucket isn't set then only 
# local backups will be made)
if config['bucket']:

  # prune old s3 backups as well, first get a list
  # of files in the s3 path
  s3files = Popen([s3cmd_path, "ls", "s3://%s/%s/*" % (config['bucket'], config['name'])], stdout=PIPE).communicate()[0].split("\n")

  # time format
  format = '%Y-%m-%d %H:%M'
  
  # make an array, it'll be use to store the most recent file names
  saveFile = []

  # loop X number of times where X is the number of backups to keep
  if (int(config['keep_last_backups']) > 0):
    for x in range(int(config['keep_last_backups'])):
    
      # clear vars used to determine recent files
      mostRecentFile = ""
      mostRecent = 0
      
      # loop through all backups in s3 backup dir
      for i in s3files:
      
        # ignore null filename strings
        if not i.strip(): continue
        
        # parse s3 file data
        fileData = i.split(" ")
        
        # get file creation time
        filetime = time.mktime(time.strptime(fileData[0].strip() + " " + fileData[1].strip(), format))
        
        # get filename
        file = fileData[len(fileData) - 1].strip()
      
        # make sure this file isn't already flagged as a recent
        # backup and  if this file has a more recent time then 
        # last set a var to let us know      
        if filetime > mostRecent and not file in saveFile:
           mostRecent = filetime
           mostRecentFile = file
       
      # after the loop append the latest most recent file     
      if mostRecentFile:  
        saveFile.append(mostRecentFile)

    # loop through all backups one more time
    # this time we'll delete anything that isn't
    # in our list of recent files
    for i in s3files:
    
      # ignore null filename strings
      if not i.strip(): continue
      
      # parse s3 file data
      fileData = i.split(" ")
      
      # get file name
      file = fileData[len(fileData) - 1].strip()

      # ignore files in recent file list
      if file in saveFile: continue
      
      # delete file on s3
      os.system("%s del %s" % (s3cmd_path, file))
    
# push backups to s3
if config['bucket']:
  print "Pushing backup to Amazon S3..."
  
  # change dir to backup path
  os.chdir(backupPath)
  
  # push files to s3 via the shell
  os.system("%s put %s s3://%s/%s/%s" % (s3cmd_path, thisFile, config['bucket'], config['name'], thisFile))

print "Done!"
